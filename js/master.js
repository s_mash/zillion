//Scripts for loader animation 
//Available for all pages
var LoadingScreen = {};
(function() {
  var checkmarkIdPrefix = "defaultSplash-checkSVG-";
  var whiteCheckmarkIdPrefix = "defaultSplash-whiteCheckSVG-";
  var checkmarkCircleIdPrefix = "defaultSplash-checkCircleSVG-";
  var phraseIdPrefix = "defaultSplash-phraseText-";
  var verticalSpacing = 50;
  var secondsPerSlide = 1.5;

  // Loop the phrases to cover the longest load time possible in the browser.
  var phrases = shuffleArray(getPhrases());
  while (phrases.length > 0 && phrases.length < 70) {
    phrases = phrases.concat(phrases);
  }

  function getPhrases() {

    var timeboxed_messages = [
      // Leave these defaults in place to be sure to serve messages.
      {
        name: "default",
        messages: [
          "EmPowering Startups",
		  "Mobile",
          "Web",
          "Analytics",
		  "Android",
		  "latest Technology",
          "Mongo DB",
          "ExpressJS",
          "Angular JS",
          "Node JS",
		  "Chart JS",
		  "Responsive Designs",
		  "Real Time Everything",

        ]
      }

    ];

    var messages_to_display = [];
    var now = new Date().getTime();
    timeboxed_messages.forEach(function(message_set) {
      if ((message_set.start === undefined || now >= message_set.start) &&
      (message_set.end === undefined || now < message_set.end)) {
        if (message_set.replaceAll) {
          messages_to_display = message_set.messages;
        } else {
          messages_to_display = messages_to_display.concat(message_set.messages);
        }
      }
    });
    return messages_to_display;
  }

  function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
      var j = Math.floor(Math.random() * (i + 1));
      var temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }

  function createSVG(tag, properties, opt_children) {
    var newElement = document.createElementNS("http://www.w3.org/2000/svg", tag);
    for(prop in properties) {
      newElement.setAttribute(prop, properties[prop]);
    }
    if (opt_children) {
      opt_children.forEach(function(child) {
        newElement.appendChild(child);
      })
    }
    return newElement;
  }

  function createPhraseSvg(phrase, index, yOffset) {
    var text = createSVG("text", {
      id: phraseIdPrefix + index,
      fill: "rgba(35,44,59,1)",
      x: 50,
      y: yOffset,
      "font-size": 18,
      "font-family": "arial"
    });
    text.appendChild(document.createTextNode(phrase + "..."));
    return text;
  }

  function createCheckSvg(yOffset, index) {
    var check = createSVG("polygon", {
      points: "21.661,7.643 13.396,19.328 9.429,15.361 7.075,17.714 13.745,24.384 24.345,9.708 ",
      fill: "rgba(35,44,59,1)",
      id: checkmarkIdPrefix + index
    });
    var white_check = createSVG("polygon", {
      points: "21.661,7.643 13.396,19.328 9.429,15.361 7.075,17.714 13.745,24.384 24.345,9.708 ",
      fill: "white",
      opacity: 0,
      id: whiteCheckmarkIdPrefix + index
    });
    var circle_outline = createSVG("path", {
      d: "M16,0C7.163,0,0,7.163,0,16s7.163,16,16,16s16-7.163,16-16S24.837,0,16,0z M16,30C8.28,30,2,23.72,2,16C2,8.28,8.28,2,16,2 c7.72,0,14,6.28,14,14C30,23.72,23.72,30,16,30z",
      fill: "rgba(35,44,59,1)"
    })
    var circle = createSVG("circle", {
      id: checkmarkCircleIdPrefix + index,
      fill: "rgba(35,44,59,1)",
      opacity: 0,
      cx: 16,
      cy: 16,
      r: 15
    })
    var group = createSVG("g", {
      transform: "translate(10 " + (yOffset - 20) + ") scale(.9)"
    }, [circle, check, circle_outline, white_check]);
    return group;
  }

  function addPhrasesToDocument(phrases) {
    phrases.forEach(function(phrase, index) {
      var yOffset = 30 + verticalSpacing * index;
      document.getElementById("defaultSplash-phrases").appendChild(createPhraseSvg(phrase, index, yOffset));
      document.getElementById("defaultSplash-phrases").appendChild(createCheckSvg(yOffset, index));
    });
  }

  /**
   * We generate the css for keyframes in javascript in order to
   * 1. Minimize page load time (with many phrases these take up many lines)
   * 2. Scale appropriately as phrases are added/removed.
   * 3. Make everything well factored. This could be done in sass (loops) but
   * would then require an additional build step. (And we'd have to maintain
   * consistency in the phrase count between the two.)
   */
  function generateKeyframeCSS(num_keyframes) {
    var generated_keyframes_style = document.createElement("style");
    generated_keyframes_style.type = "text/css";
    document.getElementsByTagName("head")[0].appendChild(generated_keyframes_style);
    function enumerateSlideUpKeyframes() {
      var style = "";
      for (var i = 0; i < num_keyframes + 1; i++) {
        style += "\
            " + (i * 100 / num_keyframes) + "% {\
                -webkit-transform: translateY(-" + (verticalSpacing * i) + "px);\
                        transform: translateY(-" + (verticalSpacing * i) + "px);\
            } ";
      }
      return style;
    }
    function enumerateFadeKeyframes() {
      var style = "";
      for (var i = 0; i < num_keyframes + 1; i++) {
        style += "#" + checkmarkCircleIdPrefix + i + " { \
          animation: fade-opacity-in 5000ms;\
          animation-delay: " + ((i - 1.5) * secondsPerSlide) + "s;\
          opacity: 0;\
        }";
        style += "#" + whiteCheckmarkIdPrefix + i + " { \
          animation: fade-opacity-in 5000ms;\
          animation-delay: " + ((i - 1.5) * secondsPerSlide) + "s;\
        }";
      }
      return style;
    }
    var animation_duration = secondsPerSlide * num_keyframes;
    var slide_up_keyframes =  enumerateSlideUpKeyframes();
    var style_rule = "@-webkit-keyframes slide-phrases-upward { " + slide_up_keyframes + " }" +
        "@keyframes slide-phrases-upward { " + slide_up_keyframes + " }";
    style_rule += " #defaultSplash-phrases {\
      -webkit-animation: slide-phrases-upward " + animation_duration + "s;\
              animation: slide-phrases-upward " + animation_duration + "s;\
    }";
    style_rule += enumerateFadeKeyframes();
    generated_keyframes_style.innerHTML = style_rule;
  }

  function initializeLoading() {
    addPhrasesToDocument(phrases);

    function getBrowser() {
      var N=navigator.appName, ua=navigator.userAgent, tem;
      var M=ua.match(/(opera|chrome|safari|firefox|msie|trident)\/?\s*(\.?\d+(\.\d+)*)/i);

      if(M && (tem= ua.match(/version\/([\.\d]+)/i))!= null) M[2]= tem[1];
      M=M? [M[1], M[2]]: [N, navigator.appVersion, '-?'];

      return M[0];
    }
    var browser = getBrowser();
    if (browser === "MSIE" || browser === "Trident") {
      /**
       * IE doesn't support animations on SVG elements, so we manually animate
       * the upwards scroll.
       */
      var upward_moving_group = document.getElementById("defaultSplash-phrases");
      upward_moving_group.currentY = 0;
      var last_time = new Date().getTime();
      function manuallyAnimateLoading() {
        var current_time = new Date().getTime();
        upward_moving_group.setAttribute("transform",
            "translate(0 " + upward_moving_group.currentY + ")");
        upward_moving_group.currentY -= verticalSpacing * (current_time - last_time) / (1000 * secondsPerSlide);
        if (upward_moving_group.currentY > -phrases.length * verticalSpacing) {
          requestAnimationFrame(manuallyAnimateLoading);
        }
        last_time = current_time;
      }
      manuallyAnimateLoading();
    }

    generateKeyframeCSS(phrases.length);



    /**
     * Transition to the app once it loads. Apparently this doesn't detect if
     * LunaUI is completely loaded, but it should be good enough. After we fade
     * out remove the loader from the dom so as to prevent any possible breakage
     * via covering the entire app with a transparent screen.
     */
    (function fadeOutOnUILoaded() {
      if (document.getElementById("asana_main") !== null) {
        var default_splash = document.getElementById("loading_screen");
        default_splash.className =
            default_splash.className + " defaultSplash--fadeOutLoading";

        default_splash.addEventListener("animationend", removeDefaultSplash,
            false);
        // Safety so that even if animation end doesn't fire we still hide.
        window.setTimeout(removeDefaultSplash, 1000);
      } else {
        window.setTimeout(fadeOutOnUILoaded, 100);
      }
    })();
  }
  initializeLoading();
})();


__FILE__="(none)";page_load_globals = {
  start_millis: new Date().getTime(),
  perf_test_prefix: "",
  times: {    scripts_started: new Date().getTime()  }};

__FILE__="(none)";
app_load_globals = {"is_server":false};
profiler_config = {
  sections: [
    {
      description:         page_load_globals.perf_test_prefix +
        "page_load.1_client",
      is_summary: true
    },
    {
      description: "~ 0 loading framework js",
      is_summary: true
    }
  ],
  start_millis: page_load_globals.start_millis
};

__FILE__="(none)";globals = window;_sts = []; _stc = function() { };modules = {};

__FILE__="(none)";  config_override_profiler = false;


//Google Analytics
//Available For All pages
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','http://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68985821-1', 'auto');
  ga('send', 'pageview');


//Wow Animations 
var wow = new WOW(
            {
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       100,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true,       // act on asynchronously loaded content (default is true)
                callback:     function(box) {
                  // the callback is fired every time an animation is started
                  // the argument that is passed in is the DOM node being animated
                }
            }
        );
    wow.init();
    
    
//Script to move to a div on another page 
// var jump=function(e)
// {
//    if (e){
//        e.preventDefault();
//        var target = $(this).attr("href");
//    }else{
//        var target = location.hash;
//    }

//    $('html,body').animate(
//    {
//        scrollTop: $(target).offset().top
//    },'slow',function()
//    {
//        location.hash = target;
//    });

// }

// $('html, body').hide();

// $(document).ready(function()
// {
//     $('a[href^=#]').bind("click", jump);

//     if (location.hash){
//         setTimeout(function(){
//             $('html, body').scrollTop(0).show();
//             jump();
//         }, 0);
//     }else{
//         $('html, body').show();
//     }
// });